﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Nextlevel : MonoBehaviour
{
    [SerializeField] private GameObject enemyShip01;
    [SerializeField] private GameObject playerShip01;
    [SerializeField] private GameObject nextLevelUI;
    [SerializeField] private GameObject hpUI;
    [SerializeField] private Text enemy01;
    [SerializeField] private int enemyHP01 = 100;
    private float nomalTimeScale = 1;

    // Start is called before the first frame update
    void Start()
    {
        Button button = gameObject.GetComponent<Button>();
        button.onClick.AddListener(Levelnext);
    }
    private void Levelnext()
    {
        hpUI.SetActive(true);
        enemyShip01.SetActive(true);
        playerShip01.SetActive(true);

        nextLevelUI.SetActive(false);

        enemy01.text = "EnemyHP :";

        Time.timeScale = nomalTimeScale;
    }
}
