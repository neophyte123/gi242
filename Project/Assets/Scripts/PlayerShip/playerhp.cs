﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class playerhp : MonoBehaviour
{
    [SerializeField] private Text playerHPUI;
    [SerializeField] private GameObject enemy01;
    [SerializeField] private GameObject player01;
    [SerializeField] private int hpPlayer = 100;
    [SerializeField] private GameObject deadUI;
    private int damageHit = 100;
    private int stopTime = 0;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "enemyshoot")
        {
            hpPlayer -= damageHit;
            Debug.Log(hpPlayer);
            playerHPUI.text = "PlayerHp : " + hpPlayer;
        }
        if (hpPlayer <= 0)
        {
            player01.SetActive(false);
            deadUI.SetActive(true);
            Time.timeScale = stopTime;
        }
    }
}
